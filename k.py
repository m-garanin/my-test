#!/usr/bin/env python
import time, random

from pymouse import PyMouse

m = PyMouse()


def run():
    while True:
        one_iter()
        time.sleep(30)
        

def one_iter():
    px, py = m.position()
    mx = random.randint(100, 300)
    for x in range(10):
        m.move(mx+x, random.randint(0, 10) )
    m.click(-11,-11, n=random.randint(1,5))
    m.move(px, py)
    
run()
